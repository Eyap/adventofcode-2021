﻿using AoCHelper;
using System;
using System.Collections;
using System.Linq;

namespace AdventOfCode
{
    public class Day_09 : TestableBaseDay
    {
        public struct Position
        {
            public int i;
            public int j;

            public Position(int i, int j)
            {
                this.i = i;
                this.j = j;
            }
        }

        private readonly int[,] _input;
        private List<Position> _lowPoints;

        public Day_09()
        {
            string[] input = File.ReadAllLines(InputFilePath);
            _input = new int[input.Length, input[0].Length];
            for (int i = 0; i < _input.GetLength(0); i++)
            {
                for (int j = 0; j < _input.GetLength(1); j++)
                {
                    _input[i, j] = int.Parse(input[i][j].ToString());
                }
            }
            _lowPoints = FindLowPoints(_input);
        }

        public override ValueTask<string> Solve_1()
        {
            int sum = 0;
            for (int lowIndex = 0; lowIndex < _lowPoints.Count; lowIndex++)
            {
                sum += _input[_lowPoints[lowIndex].i, _lowPoints[lowIndex].j] + 1;
            }
            return new(sum.ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            List<int> largestBasinsSizes = new List<int>();
            largestBasinsSizes.Add(GetSizeBasin(_lowPoints[0]));
            largestBasinsSizes.Add(GetSizeBasin(_lowPoints[1]));
            largestBasinsSizes.Add(GetSizeBasin(_lowPoints[2]));
            largestBasinsSizes.Sort();
            for (int k = 3; k < _lowPoints.Count; k++)
            {
                int size = GetSizeBasin(_lowPoints[k]);
                if (size > largestBasinsSizes.First())
                {
                    largestBasinsSizes[0] = size;
                    largestBasinsSizes.Sort();
                }

            }
            return new((largestBasinsSizes[0] * largestBasinsSizes[1] * largestBasinsSizes[2]).ToString());
        }

        private List<Position> FindLowPoints(int[,] input)
        {
            List<Position> positions = new List<Position>();
            for (int i = 0; i < input.GetLength(0); i++)
            {
                for (int j = 0; j < input.GetLength(1); j++)
                {
                    List<Position> neighbors = GetNeighbors(new Position(i, j));
                    if (!neighbors.Any(n => input[n.i, n.j] <= input[i, j]))
                    {
                        positions.Add(new Position(i, j));

                    }
                }
            }
            return positions;
        }

        private List<Position> GetNeighbors(Position pos)
        {
            List<Position> neighbors = new List<Position>();
            if (pos.i > 0)
            {
                neighbors.Add(new Position(pos.i - 1, pos.j));
            }
            if (pos.i < _input.GetLength(0) - 1)
            {
                neighbors.Add(new Position(pos.i + 1, pos.j));
            }
            if (pos.j > 0)
            {
                neighbors.Add(new Position(pos.i, pos.j - 1));
            }
            if (pos.j < _input.GetLength(1) - 1)
            {
                neighbors.Add(new Position(pos.i, pos.j + 1));
            }
            return neighbors;
        }

        private int GetSizeBasin(Position lowPoint)
        {
            Queue<Position> whiteList = new Queue<Position>();
            List<Position> basin = new List<Position>();
            whiteList.Enqueue(lowPoint);
            basin.Add(lowPoint);
            while (whiteList.Count > 0)
            {
                Position pos = whiteList.Dequeue();
                List<Position> neighbors = GetNeighbors(pos);
                for (int k = 0; k < neighbors.Count; k++)
                {
                    if (_input[neighbors[k].i, neighbors[k].j] != 9 && _input[neighbors[k].i, neighbors[k].j] >= _input[pos.i, pos.j] && !basin.Contains(neighbors[k]))
                    {
                        whiteList.Enqueue(neighbors[k]);
                        basin.Add(neighbors[k]);
                    }
                }
            }
            return basin.Count;
        }
    }

}

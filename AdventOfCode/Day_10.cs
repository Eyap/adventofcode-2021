﻿namespace AdventOfCode
{
    using AoCHelper;
    using System;
    using System.Collections;
    using System.Linq;

    public class Day_10 : TestableBaseDay
    {
        private readonly string[] _input;

        private readonly char[] _openChunkCharacters = new char[] { '(', '[', '{', '<' };
        private readonly char[] _closeChunkCharacters = new char[] { ')', ']', '}', '>' };
        private readonly int[] _closeChunkScores = new int[] { 3, 57, 1197, 25137 };

        private List<string> _inputWithoutCorrupted;
        private int _corruptionScore;
        private List<Int64> _completionScores;

        public Day_10()
        {
            _input = File.ReadAllLines(InputFilePath);

            _inputWithoutCorrupted = GetWithoutCorrupted(_input, out _corruptionScore, out _completionScores);
        }

        public override ValueTask<string> Solve_1()
        {
            return new(_corruptionScore.ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            _completionScores.Sort();
            return new(_completionScores[_completionScores.Count / 2].ToString());
        }

        List<string> GetWithoutCorrupted(string[] input, out int sumCorruptScore, out List<Int64> completionScores)
        {
            sumCorruptScore = 0;
            completionScores = new List<Int64>();
            List<string> result = new List<string>();
            for (int i = 0; i < input.Length; i++)
            {
                if (IsCorrupted(_input[i], out int corruptScore, out Int64 completionScore))
                {
                    sumCorruptScore += corruptScore;
                }
                else
                {
                    completionScores.Add(completionScore);
                    result.Add(_input[i]);
                }
            }
            return result;
        }

        bool IsCorrupted(string line, out int corruptScore, out Int64 completionScore)
        {
            corruptScore = 0;
            completionScore = 0;
            Stack<char> openChunks = new Stack<char>();
            for (int i = 0; i < line.Length; i++)
            {
                if (_openChunkCharacters.Contains(line[i]))
                {
                    openChunks.Push(line[i]);
                }
                else if (_closeChunkCharacters.Contains(line[i]))
                {
                    int indexClosingCharacter = Array.IndexOf(_closeChunkCharacters, line[i]);
                    if (openChunks.Peek() == _openChunkCharacters[indexClosingCharacter])
                    {
                        openChunks.Pop();
                    }
                    else
                    {
                        corruptScore = _closeChunkScores[indexClosingCharacter];
                        return true; // Corrupted !
                    }
                }
                else
                {
                    throw new ArgumentException("Character not recognized"); // Wrong character !
                }
            }
            completionScore = GetCompletionScore(openChunks);
            return false;
        }

        Int64 GetCompletionScore(Stack<char> openChunks)
        {
            Int64 lineScore = 0;
            while (openChunks.Count > 0)
            {
                lineScore = lineScore * 5 + 1 + Array.IndexOf(_openChunkCharacters, openChunks.Pop());
            }
            return lineScore;
        }
    }
}

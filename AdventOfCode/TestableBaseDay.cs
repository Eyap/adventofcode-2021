using AoCHelper;

public abstract class TestableBaseDay : BaseDay
{
    public string TestInputFilePath { private get; set; }
    public override string InputFilePath => TestInputFilePath;

    protected TestableBaseDay()
    {
        TestInputFilePath = base.InputFilePath;
    }
}

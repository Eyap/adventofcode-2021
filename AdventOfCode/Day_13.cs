﻿namespace AdventOfCode
{
    using AoCHelper;
    using System;
    using System.Collections;
    using System.Linq;
    using System.Text.RegularExpressions;

    public class Day_13 : TestableBaseDay
    {
        public struct Position
        {
            public int x;
            public int y;

            public Position(string line)
            {
                string[] l = line.Split(',');
                this.x = int.Parse(l[0]);
                this.y = int.Parse(l[1]);
            }
            public Position(int x, int y)
            {
                this.x = x;
                this.y = y;
            }
            public override string ToString()
            {
                return string.Format("{0},{1}", x.ToString(), y.ToString());
            }
        }
        public struct FoldInstruction
        {
            public bool isXFolding;
            public int foldLine;

            public FoldInstruction(string line)
            {
                Match m = Regex.Match(line, @"fold along ([xy])=(\d+)");
                if (!m.Success)
                {
                    throw new Exception("Error when parsing input");
                }
                this.isXFolding = m.Groups[1].Value == "x";
                this.foldLine = int.Parse(m.Groups[2].Value);
            }

            public override string ToString()
            {
                return string.Format("fold along {0}={1}", isXFolding ? "x" : "y", foldLine.ToString());
            }
        }

        private readonly string[] _input;
        private readonly List<Position> _initialPositions;
        private readonly List<FoldInstruction> _foldInstructions;

        public Day_13()
        {
            _input = File.ReadAllLines(InputFilePath);
            _initialPositions = new List<Position>();
            _foldInstructions = new List<FoldInstruction>();
            for (int i = 0; i < _input.Length; i++)
            {
                if (string.IsNullOrWhiteSpace(_input[i]))
                {
                    // Console.WriteLine("Empty Input");
                    // Do Nothing
                }
                else if (_input[i].StartsWith("fold along"))
                {
                    FoldInstruction newFoldInstruction = new FoldInstruction(_input[i]);
                    // Console.WriteLine(newFoldInstruction.ToString());
                    _foldInstructions.Add(newFoldInstruction);
                }
                else
                {
                    Position newPosition = new Position(_input[i]);
                    // Console.WriteLine(newPosition.ToString());
                    _initialPositions.Add(newPosition);
                }
            }
        }

        public override ValueTask<string> Solve_1()
        {
            return new(FoldAlongXY(_initialPositions, _foldInstructions[0]).Count.ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            List<Position> positions = _initialPositions;
            foreach (FoldInstruction foldInstruction in _foldInstructions)
            {
                positions = FoldAlongXY(positions, foldInstruction);
            }
            // Display them - NOT OPTIMIZED
            // for (int i = 0; i < 6; i++)
            // {
            //     for (int j = 0; j < 39; j++)
            //     {
            //         Console.Write(positions.Contains(new Position(j, i)) ? "#" : ".");
            //     }
            //     Console.WriteLine("");
            // }
            return new("Need to Enable display to see answer");
        }

        public List<Position> FoldAlongXY(List<Position> _initialPositions, FoldInstruction foldInstruction)
        {
            List<Position> result = new List<Position>();
            for (int i = 0; i < _initialPositions.Count; i++)
            {
                if (foldInstruction.isXFolding && _initialPositions[i].x == foldInstruction.foldLine || !foldInstruction.isXFolding && _initialPositions[i].y == foldInstruction.foldLine)
                {
                    Console.WriteLine(string.Format("On Fold line ({0}). Skipped", foldInstruction.foldLine));
                }
                else if (foldInstruction.isXFolding && _initialPositions[i].x > foldInstruction.foldLine)
                {
                    result.Add(new Position(_initialPositions[i].x - 2 * (_initialPositions[i].x - foldInstruction.foldLine), _initialPositions[i].y));
                }
                else if (!foldInstruction.isXFolding && _initialPositions[i].y > foldInstruction.foldLine)
                {
                    result.Add(new Position(_initialPositions[i].x, _initialPositions[i].y - 2 * (_initialPositions[i].y - foldInstruction.foldLine)));
                }
                else
                {
                    result.Add(_initialPositions[i]);
                }
            }
            return result.Distinct().ToList(); // Not Optimized (recreation of a list)
        }
    }
}

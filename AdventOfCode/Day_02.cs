﻿using AoCHelper;

namespace AdventOfCode
{
    public class Day_02 : TestableBaseDay
    {
        private readonly string[] _input;

        public Day_02()
        {
            _input = File.ReadAllLines(InputFilePath);
        }

        public override ValueTask<string> Solve_1()
        {
            int currentDepth = 0;
            int currentHorizontalPosition = 0;
            for (int i = 0; i < _input.Length; i++)
            {
                string[] commands = _input[i].Split(' ');
                switch (commands[0])
                {
                    case "forward":
                        currentHorizontalPosition += int.Parse(commands[1]);
                        break;
                    case "down":
                        currentDepth += int.Parse(commands[1]);
                        break;
                    case "up":
                        currentDepth -= int.Parse(commands[1]);
                        break;
                    default:
                        throw new NotImplementedException();
                }
            }
            return new((currentDepth * currentHorizontalPosition).ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            int currentDepth = 0;
            int currentHorizontalPosition = 0;
            int aim = 0;
            for (int i = 0; i < _input.Length; i++)
            {
                string[] commands = _input[i].Split(' ');
                switch (commands[0])
                {
                    case "forward":
                        currentHorizontalPosition += int.Parse(commands[1]);
                        currentDepth += aim * int.Parse(commands[1]);
                        break;
                    case "down":
                        aim += int.Parse(commands[1]);
                        break;
                    case "up":
                        aim -= int.Parse(commands[1]);
                        break;
                    default:
                        throw new NotImplementedException();
                }
            }
            return new((currentDepth * currentHorizontalPosition).ToString());
        }
    }
}

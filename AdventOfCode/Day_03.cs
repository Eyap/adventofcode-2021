﻿using AoCHelper;
using System;
using System.Collections;

namespace AdventOfCode
{
    /// <summary>
    /// Note for possible improvements :
    /// I decided to keep using char ('1' and '0') in the resolution of the problem.
    /// It could be improved (performance-wise) by using booleans. 
    /// For that, the _input could be directly parsed into a matrix of bool.
    /// It may (or may not ?) also improve the readability a bit.
    /// </summary>
    public class Day_03 : TestableBaseDay
    {
        private readonly string[] _input;

        public Day_03()
        {
            _input = File.ReadAllLines(InputFilePath);
        }

        public override ValueTask<string> Solve_1()
        {
            string gammaRate = "";
            string epsilonRate = "";

            int lengthDiagnosticNumber = _input[0].Length;
            int[] counterOnes = new int[lengthDiagnosticNumber];
            for (int i = 0; i < _input.Length; i++)
            {
                for (int j = 0; j < lengthDiagnosticNumber; j++)
                {
                    if (_input[i][j] == '1')
                    {
                        counterOnes[j]++;
                    }
                }
            }

            for (int j = 0; j < lengthDiagnosticNumber; j++)
            {
                gammaRate += counterOnes[j] >= _input.Length / 2.0 ? '1' : '0';
                epsilonRate += counterOnes[j] <= _input.Length / 2.0 ? '1' : '0';
            }
            return new((Convert.ToInt32(gammaRate, 2) * Convert.ToInt32(epsilonRate, 2)).ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            string oxygenGeneratorRating = GetLifeSupportRating(_input, true);
            string co2ScrubberRating = GetLifeSupportRating(_input, false);

            return new((Convert.ToInt32(oxygenGeneratorRating, 2) * Convert.ToInt32(co2ScrubberRating, 2)).ToString());
        }

        static string GetLifeSupportRating(string[] input, bool getOxygenRating)
        {
            List<string> inputList = input.ToList();

            int i = 0;
            while (inputList.Count > 1 && i < input[0].Length)
            {
                char mask = GetMask(inputList, i, getOxygenRating, getOxygenRating ? '1' : '0');
                for (int j = inputList.Count - 1; j >= 0; j--)
                {
                    if (inputList[j][i] != mask)
                    {
                        inputList.RemoveAt(j);
                    }
                }
                i++;
            }

            if (inputList.Count != 1)
            {
                throw new Exception();
            }
            return inputList[0];
        }

        static char GetMask(List<string> input, int bitIndex, bool useMost, char roundingChar)
        {
            int counter = 0;
            for (int i = 0; i < input.Count; i++)
            {
                if (input[i][bitIndex] == '1')
                {
                    counter++;
                }
            }
            if (counter == input.Count / 2.0)
            {
                return roundingChar;
            }
            else
            {
                if (counter > input.Count / 2.0)
                {
                    return useMost ? '1' : '0';
                }
                else
                {
                    return useMost ? '0' : '1';
                }
            }
        }

        /// <summary>
        /// ALTERNATIVE WAY OF DOING THIS
        /// Clearly not the best way to solve this, but I wanted to discover the BitArray class.
        /// </summary>
        /// <returns></returns>
        // public override ValueTask<string> Solve_1()
        // {
        //     int lengthDiagnosticNumber = _input[0].Length;
        //     int[] counterOnes = new int[lengthDiagnosticNumber];
        //     for (int i = 0; i < _input.Length; i++)
        //     {
        //         for (int j = 0; j < lengthDiagnosticNumber; j++)
        //         {
        //             if (_input[i][j] == '1')
        //             {
        //                 counterOnes[j]++;
        //             }
        //         }
        //     }

        //     bool[] gammaRateBool = new bool[lengthDiagnosticNumber];
        //     for (int j = 0; j < lengthDiagnosticNumber; j++)
        //     {
        //         gammaRateBool[lengthDiagnosticNumber - 1 - j] = counterOnes[j] > _input.Length / 2;
        //     }
        //     Console.WriteLine(gammaRateBool[1].ToString());
        //     BitArray gammaRate = new BitArray(gammaRateBool);
        //     Console.WriteLine(gammaRate.ToString());
        //     return new((ConvertToInt(gammaRate) * ConvertToInt(gammaRate.Not())).ToString());
        // }

        // int ConvertToInt(BitArray binary)
        // {
        //     if (binary == null)
        //         throw new ArgumentNullException("binary");
        //     if (binary.Length > 32)
        //         throw new ArgumentException("must be at most 32 bits long");

        //     var result = new int[1];
        //     binary.CopyTo(result, 0);
        //     return result[0];
        // }
    }
}

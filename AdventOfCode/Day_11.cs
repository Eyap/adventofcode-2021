﻿namespace AdventOfCode
{
    using AoCHelper;
    using System;
    using System.Collections;
    using System.Linq;

    public class Day_11 : TestableBaseDay
    {
        public struct Position
        {
            public int i;
            public int j;

            public Position(int i, int j)
            {
                this.i = i;
                this.j = j;
            }
        }

        private readonly int[,] _input;

        public Day_11()
        {
            string[] input = File.ReadAllLines(InputFilePath);
            _input = new int[input.Length, input[0].Length];
            for (int i = 0; i < _input.GetLength(0); i++)
            {
                for (int j = 0; j < _input.GetLength(1); j++)
                {
                    _input[i, j] = int.Parse(input[i][j].ToString());
                }
            }
        }

        public override ValueTask<string> Solve_1()
        {
            return new(GetFlashCount(_input, 100).ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            return new(GetFirstSync(_input).ToString());
        }

        public int GetFlashCount(int[,] initialState, int totalDays)
        {
            int[,] currentStep = (int[,])initialState.Clone();
            int flashCount = 0;
            Queue<Position> flashingPositions = new Queue<Position>();
            List<Position> flashedPositions = new List<Position>();
            for (int day = 0; day < totalDays; day++)
            {
                flashedPositions.Clear();
                // Perform Step
                for (int i = 0; i < currentStep.GetLength(0); i++)
                {
                    for (int j = 0; j < currentStep.GetLength(1); j++)
                    {
                        currentStep[i, j]++;
                        if (currentStep[i, j] > 9)
                        {
                            flashingPositions.Enqueue(new Position(i, j));
                        }
                    }
                }

                // Do all flashes
                while (flashingPositions.Count > 0)
                {
                    Position flashingPos = flashingPositions.Dequeue();
                    flashedPositions.Add(flashingPos);
                    flashCount++;
                    List<Position> neighbors = GetNeighbors(flashingPos);
                    foreach (Position neighborPosition in neighbors)
                    {
                        currentStep[neighborPosition.i, neighborPosition.j]++;
                        if (currentStep[neighborPosition.i, neighborPosition.j] > 9 && !flashedPositions.Contains(neighborPosition) && !flashingPositions.Contains(neighborPosition))
                        {
                            flashingPositions.Enqueue(neighborPosition);
                        }
                    }
                }
                // Reset all flashed to 0
                foreach (Position flashedPos in flashedPositions)
                {
                    currentStep[flashedPos.i, flashedPos.j] = 0;
                }

                // DisplayMatrix(currentStep);
                // Console.WriteLine(flashCount);
            }
            return flashCount;
        }

        // Lazy copy of above method
        public int GetFirstSync(int[,] initialState)
        {
            int[,] currentStep = (int[,])initialState.Clone();
            int day = 0;
            Queue<Position> flashingPositions = new Queue<Position>();
            List<Position> flashedPositions = new List<Position>();
            while (flashedPositions.Count < initialState.GetLength(0) * initialState.GetLength(1))
            {
                flashedPositions.Clear();
                // Perform Step
                for (int i = 0; i < currentStep.GetLength(0); i++)
                {
                    for (int j = 0; j < currentStep.GetLength(1); j++)
                    {
                        currentStep[i, j]++;
                        if (currentStep[i, j] > 9)
                        {
                            flashingPositions.Enqueue(new Position(i, j));
                        }
                    }
                }

                // Do all flashes
                while (flashingPositions.Count > 0)
                {
                    Position flashingPos = flashingPositions.Dequeue();
                    flashedPositions.Add(flashingPos);
                    List<Position> neighbors = GetNeighbors(flashingPos);
                    foreach (Position neighborPosition in neighbors)
                    {
                        currentStep[neighborPosition.i, neighborPosition.j]++;
                        if (currentStep[neighborPosition.i, neighborPosition.j] > 9 && !flashedPositions.Contains(neighborPosition) && !flashingPositions.Contains(neighborPosition))
                        {
                            flashingPositions.Enqueue(neighborPosition);
                        }
                    }
                }
                // Reset all flashed to 0
                foreach (Position flashedPos in flashedPositions)
                {
                    currentStep[flashedPos.i, flashedPos.j] = 0;
                }

                day++;
                // DisplayMatrix(currentStep);
                // Console.WriteLine(flashCount);
            }
            return day;
        }

        private List<Position> GetNeighbors(Position pos)
        {
            List<Position> neighbors = new List<Position>();
            if (pos.i > 0)
            {
                neighbors.Add(new Position(pos.i - 1, pos.j));
                if (pos.j > 0)
                {
                    neighbors.Add(new Position(pos.i - 1, pos.j - 1));
                }
                if (pos.j < _input.GetLength(1) - 1)
                {
                    neighbors.Add(new Position(pos.i - 1, pos.j + 1));
                }
            }
            if (pos.i < _input.GetLength(0) - 1)
            {
                neighbors.Add(new Position(pos.i + 1, pos.j));
                if (pos.j > 0)
                {
                    neighbors.Add(new Position(pos.i + 1, pos.j - 1));
                }
                if (pos.j < _input.GetLength(1) - 1)
                {
                    neighbors.Add(new Position(pos.i + 1, pos.j + 1));
                }
            }
            if (pos.j > 0)
            {
                neighbors.Add(new Position(pos.i, pos.j - 1));
            }
            if (pos.j < _input.GetLength(1) - 1)
            {
                neighbors.Add(new Position(pos.i, pos.j + 1));
            }

            return neighbors;
        }

        private void DisplayMatrix(int[,] currentStep)
        {
            for (int i = 0; i < currentStep.GetLength(0); i++)
            {
                for (int j = 0; j < currentStep.GetLength(1); j++)
                {
                    Console.Write(currentStep[i, j]);
                }
                Console.WriteLine();
            }
        }
    }
}

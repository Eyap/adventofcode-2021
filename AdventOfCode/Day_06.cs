﻿using AoCHelper;
using System;
using System.Collections;

namespace AdventOfCode
{
    /// <summary>
    /// Note for possible improvements :
    /// I decided to keep using char ('1' and '0') in the resolution of the problem.
    /// It could be improved (performance-wise) by using booleans. 
    /// For that, the _input could be directly parsed into a matrix of bool.
    /// It may (or may not ?) also improve the readability a bit.
    /// </summary>
    public class Day_06 : TestableBaseDay
    {
        private readonly int[] _inputFishes;

        private static readonly int _procreationDayCount = 7;

        public Day_06()
        {
            string input = File.ReadAllText(InputFilePath);
            _inputFishes = input.Split(',').Select(int.Parse).ToArray();
        }

        public override ValueTask<string> Solve_1()
        {
            return new(GetFishCountAfterReproduction(_inputFishes, 80).ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            return new(GetFishCountAfterReproduction(_inputFishes, 256).ToString());
        }

        private static Int64 GetFishCountAfterReproduction(int[] inputFishes, int numberOfDays)
        {
            int maxDays = _procreationDayCount + 2; // Total number of days for a newborn to reproduce.
            Int64[] counter = new Int64[9];

            // Initialize counter
            for (int fish = 0; fish < inputFishes.Length; fish++)
            {
                counter[inputFishes[fish]]++;
            }

            // Proper iteration and problem solving
            for (int day = 0; day < numberOfDays; day++)
            {
                counter[(_procreationDayCount + day) % maxDays] += counter[day % maxDays];
            }

            return counter.Sum();
        }
    }
}

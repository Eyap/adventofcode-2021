﻿namespace AdventOfCode
{
    using AoCHelper;
    using System;
    using System.Collections;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;

    public class Day_15 : TestableBaseDay
    {
        public struct Position : IEquatable<Position>
        {
            public int i;
            public int j;

            public Position(int i, int j)
            {
                this.i = i;
                this.j = j;
            }

            public override bool Equals(object other)
            {
                if (ReferenceEquals(null, other)) return false;
                // if (ReferenceEquals(this, other)) return true;
                if (this.GetType() != other.GetType()) return false;
                return IsEqual((Position)other);
            }

            public bool Equals(Position other)
            {
                // if (ReferenceEquals(null, other)) return false; // struct can't be null
                // if (ReferenceEquals(this, other)) return true;
                return IsEqual(other);
            }

            private bool IsEqual(Position other)
            {
                return this.i == other.i && this.j == other.j;
            }

            public override string ToString()
            {
                return string.Format("Position ({0}, {1})", i.ToString(), j.ToString());
            }

            public override int GetHashCode()
            {
                return (i, j).GetHashCode();
            }

            public static bool operator ==(Position left, Position right)
            {
                return left.Equals(right);
            }

            public static bool operator !=(Position left, Position right)
            {
                return !left.Equals(right);
            }

            public int ManhattanDistance(Position other)
            {
                int distI = Math.Abs(this.i - other.i);
                int distJ = Math.Abs(this.j - other.j);
                return distI + distJ;
            }
        }

        public struct Node : IComparable<Node>
        {
            public Position position;
            public int cost;

            public Node(Position position, int cost)
            {
                this.position = position;
                this.cost = cost;
            }

            public int CompareTo(Node other)
            {
                return this.cost.CompareTo(other.cost);
            }
        }

        private readonly int[,] _input;

        public Day_15()
        {
            string[] input = File.ReadAllLines(InputFilePath);
            _input = new int[input.Length, input[0].Length];
            for (int i = 0; i < _input.GetLength(0); i++)
            {
                for (int j = 0; j < _input.GetLength(1); j++)
                {
                    _input[i, j] = int.Parse(input[i][j].ToString());
                }
            }
        }

        public override ValueTask<string> Solve_1()
        {
            return new(AStar(new Position(0, 0), new Position(_input.GetLength(0) - 1, _input.GetLength(1) - 1), _input).Skip(1).Sum(x => _input[x.i, x.j]).ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            int[,] bigMap = new int[_input.GetLength(0) * 5, _input.GetLength(1) * 5];
            for (int i = 0; i < bigMap.GetLength(0); i++)
            {
                for (int j = 0; j < bigMap.GetLength(1); j++)
                {
                    int newRisk = _input[i % _input.GetLength(0), j % _input.GetLength(1)] + i / _input.GetLength(0) + j / _input.GetLength(1);
                    bigMap[i, j] = (newRisk - 1) % 9 + 1;
                }
            }
            return new(AStar(new Position(0, 0), new Position(bigMap.GetLength(0) - 1, bigMap.GetLength(1) - 1), bigMap).Skip(1).Sum(x => bigMap[x.i, x.j]).ToString());
        }


        public static List<Position> AStar(Position start, Position end, int[,] map)
        {
            PriorityQueue<Position, Int64> openQueue = new PriorityQueue<Position, Int64>();
            Dictionary<Position, Position> cameFromMap = new Dictionary<Position, Position>(); // Key: next, value: previous

            Dictionary<Position, int> gScore = new();
            gScore[start] = 0;

            Dictionary<Position, int> fScore = new();
            fScore[start] = 0;
            openQueue.Enqueue(start, fScore[start]);

            while (openQueue.TryDequeue(out Position currentPos, out Int64 cost))
            {
                // Console.WriteLine("Check " + node.position.ToString() + " with value of " + node.cost.ToString());
                if (currentPos.Equals(end))
                {
                    return ReconstructPath(cameFromMap, end);
                }

                foreach (Position neighborPos in GetNeighbors(currentPos, map))
                {

                    var tentGScore = gScore[currentPos] + map[neighborPos.i, neighborPos.j];
                    if (tentGScore < gScore.GetValueOrDefault(neighborPos, int.MaxValue))
                    {
                        cameFromMap[neighborPos] = currentPos;
                        gScore[neighborPos] = tentGScore;
                        fScore[neighborPos] = tentGScore + currentPos.ManhattanDistance(end);
                        openQueue.Enqueue(neighborPos, fScore[neighborPos]);
                    }
                }
            }
            Console.WriteLine("Error");
            throw new Exception();
        }

        private static List<Position> GetNeighbors(Position pos, int[,] map)
        {
            List<Position> neighbors = new List<Position>();
            if (pos.i > 0)
            {
                neighbors.Add(new Position(pos.i - 1, pos.j));
            }
            if (pos.i < map.GetLength(0) - 1)
            {
                neighbors.Add(new Position(pos.i + 1, pos.j));
            }
            if (pos.j > 0)
            {
                neighbors.Add(new Position(pos.i, pos.j - 1));
            }
            if (pos.j < map.GetLength(1) - 1)
            {
                neighbors.Add(new Position(pos.i, pos.j + 1));
            }

            return neighbors;
        }
        private static List<Position> ReconstructPath(Dictionary<Position, Position> cameFromMap, Position end)
        {
            List<Position> path = new List<Position>() { end };
            Position previous = end;
            while (cameFromMap.ContainsKey(previous))
            {
                previous = cameFromMap[previous];
                // Console.WriteLine(previous.ToString());
                path.Add(previous);
            }
            path.Reverse();
            return path;
        }
    }
}

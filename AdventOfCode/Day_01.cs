﻿using AoCHelper;

namespace AdventOfCode
{
    public class Day_01 : TestableBaseDay
    {
        private readonly string[] _input;

        public Day_01()
        {
            _input = File.ReadAllLines(InputFilePath);
        }

        public override ValueTask<string> Solve_1()
        {
            int sum = 0;
            for (int i = 1; i < _input.Length; i++)
            {
                if (int.Parse(_input[i - 1]) < int.Parse(_input[i]))
                {
                    sum++;
                }
            }
            return new(sum.ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            int sum = 0;
            int previousSlidingWindow = int.Parse(_input[0]) + int.Parse(_input[1]) + int.Parse(_input[2]);
            for (int i = 1; i < _input.Length - 2; i++)
            {
                int currentSlidingWindow = int.Parse(_input[i]) + int.Parse(_input[i + 1]) + int.Parse(_input[i + 2]);
                if (previousSlidingWindow < currentSlidingWindow)
                {
                    sum++;
                }
                previousSlidingWindow = currentSlidingWindow;
            }
            return new(sum.ToString());
        }
    }
}

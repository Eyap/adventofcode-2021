﻿using AoCHelper;
using System;
using System.Collections;
using System.Linq;

namespace AdventOfCode
{
    static class StringExtension
    {
        public static bool ContainsUnordored(this string str, string aa)
        {
            for (int i = 0; i < aa.Length; i++)
            {
                if (!str.Contains(aa[i]))
                {
                    return false;
                }
            }
            return true;
        }
    }

    public class Day_08 : TestableBaseDay
    {
        private readonly string[][] _patterns;
        private readonly string[][] _outputValues;

        public Day_08()
        {
            string[] input = File.ReadAllLines(InputFilePath);
            _patterns = new string[input.Length][];
            _outputValues = new string[input.Length][];
            for (int i = 0; i < input.Length; i++)
            {
                string[] separatedInput = input[i].Split(" | ");
                _patterns[i] = separatedInput[0].Split(' ');
                _outputValues[i] = separatedInput[1].Split(' ');
            }
        }

        public override ValueTask<string> Solve_1()
        {
            int sum = 0;
            for (int i = 0; i < _outputValues.Length; i++)
            {
                for (int j = 0; j < _outputValues[i].Length; j++)
                {
                    switch (_outputValues[i][j].Length)
                    {
                        case 2:
                        case 3:
                        case 4:
                        case 7:
                            sum++;
                            break;
                        default:
                            break;
                    }
                }
            }
            return new(sum.ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            int sum = 0;
            for (int i = 0; i < _patterns.Length; i++)
            {
                DigitsSolver solver = new DigitsSolver(_patterns[i]);
                sum += solver.Decode(_outputValues[i]);
            }
            return new(sum.ToString());
        }

        public class DigitsSolver
        {
            public string[] _decodingValues = new string[10];

            public DigitsSolver(string[] pattern)
            {
                Solve(pattern);
            }

            public int Decode(string[] fourDigits)
            {
                int sum = 0;
                for (int i = 0; i < fourDigits.Length; i++)
                {
                    sum = sum * 10 + DecodeDigit(fourDigits[i]);
                }
                return sum;
            }

            private void Solve(string[] pattern)
            {
                if (pattern.Length != 10)
                {
                    throw new ArgumentException();
                }

                List<string> digitToFind = pattern.ToList();
                // Find 1, 4, 7, 8
                for (int i = digitToFind.Count - 1; i >= 0; i--)
                {
                    switch (digitToFind[i].Length)
                    {
                        case 2:
                            _decodingValues[1] = pattern[i];
                            digitToFind.RemoveAt(i);
                            break;
                        case 3:
                            _decodingValues[7] = pattern[i];
                            digitToFind.RemoveAt(i);
                            break;
                        case 4:
                            _decodingValues[4] = pattern[i];
                            digitToFind.RemoveAt(i);
                            break;
                        case 7:
                            _decodingValues[8] = pattern[i];
                            digitToFind.RemoveAt(i);
                            break;
                        default:
                            break;
                    }
                }

                // Find 9 and 6
                for (int i = digitToFind.Count - 1; i >= 0; i--)
                {
                    if (digitToFind[i].Length == 6)
                    {
                        if (digitToFind[i].ContainsUnordored(_decodingValues[4]))
                        {
                            _decodingValues[9] = digitToFind[i];
                            digitToFind.RemoveAt(i);
                        }
                        else if (!digitToFind[i].ContainsUnordored(_decodingValues[1]))
                        {
                            _decodingValues[6] = digitToFind[i];
                            digitToFind.RemoveAt(i);
                        }
                    }
                }

                // Deduce 0, Find 3 and 5
                for (int i = digitToFind.Count - 1; i >= 0; i--)
                {
                    if (digitToFind[i].Length == 6)
                    {
                        _decodingValues[0] = digitToFind[i];
                        digitToFind.RemoveAt(i);
                    }
                    else if (digitToFind[i].ContainsUnordored(_decodingValues[1]))
                    {
                        _decodingValues[3] = digitToFind[i];
                        digitToFind.RemoveAt(i);
                    }
                    else if (_decodingValues[6].ContainsUnordored(digitToFind[i]))
                    {
                        _decodingValues[5] = digitToFind[i];
                        digitToFind.RemoveAt(i);
                    }
                }

                // Deduce last : 2
                if (digitToFind.Count != 1)
                {
                    throw new Exception("Error while decoding.");
                }
                _decodingValues[2] = digitToFind[0];
            }

            private int DecodeDigit(string digit)
            {
                for (int i = 0; i < _decodingValues.Length; i++)
                {
                    if (digit.Length == _decodingValues[i].Length && digit.ContainsUnordored(_decodingValues[i]))
                    {
                        return i;
                    }
                }
                throw new Exception("Not Found in solution");
            }
        }
    }

}

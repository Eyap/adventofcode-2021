﻿using AoCHelper;
using System;
using System.Collections;
using System.Linq;

namespace AdventOfCode
{
    public class Day_07 : TestableBaseDay
    {
        private readonly int[] _inputCrabs;
        private Dictionary<int, int> _crabsCountByPosition;

        public Day_07()
        {
            string input = File.ReadAllText(InputFilePath);
            _inputCrabs = input.Split(',').Select(int.Parse).ToArray();
            _crabsCountByPosition = new Dictionary<int, int>();

            for (int i = 0; i < _inputCrabs.Length; i++)
            {
                int crabCount;
                bool hasAlreadyCrabs = _crabsCountByPosition.TryGetValue(_inputCrabs[i], out crabCount);
                _crabsCountByPosition[_inputCrabs[i]] = hasAlreadyCrabs ? crabCount + 1 : 1;
            }
        }

        public override ValueTask<string> Solve_1()
        {
            // First try - bad way
            // int leastSumRequired = GetFuelRequired(_crabsCountByPosition, _crabsCountByPosition.ElementAt(0).Key);
            // for (int i = 1; i < _crabsCountByPosition.Count; i++)
            // {
            //     leastSumRequired = Math.Min(GetFuelRequired(_crabsCountByPosition, _crabsCountByPosition.ElementAt(i).Key), leastSumRequired);
            // }
            // return new(leastSumRequired.ToString());

            // Second method - Use of median
            List<int> crabs = _inputCrabs.ToList();
            crabs.Sort();
            int median = crabs.ElementAt((crabs.Count + 1) / 2 - 1);

            return new(GetFuelRequiredSimple(_crabsCountByPosition, median).ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            // Use of average
            double average = _inputCrabs.Average();
            int averageCeil = (int)Math.Ceiling(_inputCrabs.Average());
            int averageFloor = (int)Math.Floor(_inputCrabs.Average());

            return new(Math.Min(GetFuelRequiredComplex(_crabsCountByPosition, averageCeil), GetFuelRequiredComplex(_crabsCountByPosition, averageFloor)).ToString());
        }

        private static int GetFuelRequiredSimple(Dictionary<int, int> crabsCountByPosition, int testedPosition)
        {
            int fuelRequired = 0;
            for (int i = 0; i < crabsCountByPosition.Count; i++)
            {
                fuelRequired += Math.Abs(crabsCountByPosition.ElementAt(i).Key - testedPosition) * crabsCountByPosition.ElementAt(i).Value;
            }

            return fuelRequired;
        }

        private static int GetFuelRequiredComplex(Dictionary<int, int> crabsCountByPosition, int testedPosition)
        {
            int fuelRequired = 0;
            for (int i = 0; i < crabsCountByPosition.Count; i++)
            {
                int diff = Math.Abs(crabsCountByPosition.ElementAt(i).Key - testedPosition);
                int toAdd = diff * (diff + 1) / 2 * crabsCountByPosition.ElementAt(i).Value;
                fuelRequired += toAdd;
            }

            return fuelRequired;
        }
    }

}

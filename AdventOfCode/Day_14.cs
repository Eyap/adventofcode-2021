﻿namespace AdventOfCode
{
    using AoCHelper;
    using System;
    using System.Collections;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;

    public class Day_14 : TestableBaseDay
    {
        private readonly string[] _input;
        private readonly string _polymerTemplate;
        private readonly Dictionary<string, char> _insertionPairs;

        public Day_14()
        {
            _input = File.ReadAllLines(InputFilePath);
            _polymerTemplate = _input[0];
            _insertionPairs = new Dictionary<string, char>();
            for (int i = 2; i < _input.Length; i++)
            {
                _insertionPairs.Add(_input[i].Substring(0, 2), _input[i][6]);
            }
        }

        public override ValueTask<string> Solve_1()
        {
            var finalPolymer = GrowPolymerOptimized(_polymerTemplate, _insertionPairs, 10);
            return new(CountDifferenceOccurences(finalPolymer, _polymerTemplate.Last()).ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            var finalPolymer = GrowPolymerOptimized(_polymerTemplate, _insertionPairs, 40);
            return new(CountDifferenceOccurences(finalPolymer, _polymerTemplate.Last()).ToString());
        }

        private static string GrowPolymer(string template, Dictionary<string, char> insertionPairs, int steps)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder previousStep = new StringBuilder(template);
            for (int step = 0; step < steps; step++)
            {
                sb.Clear();
                for (int i = 0; i < previousStep.Length - 1; i++)
                {
                    sb.Append(previousStep[i]);
                    sb.Append(insertionPairs[string.Concat(previousStep[i], previousStep[i + 1])]);
                }
                sb.Append(previousStep[previousStep.Length - 1]);
                previousStep = new StringBuilder(sb.ToString());
                // Console.WriteLine(sb.ToString());
            }
            return sb.ToString();
        }

        private static Dictionary<string, Int64> GrowPolymerOptimized(string template, Dictionary<string, char> insertionPairs, int steps)
        {
            // Initialisation
            Dictionary<string, Int64> previousPairsCount = new Dictionary<string, Int64>();
            for (int i = 0; i < template.Length - 1; i++)
            {
                if (!previousPairsCount.TryAdd(string.Concat(template[i], template[i + 1]), 1))
                {
                    previousPairsCount[string.Concat(template[i], template[i + 1])]++;
                }
            }
            Dictionary<string, Int64> currentPairsCount = new Dictionary<string, Int64>();

            // Perform steps
            for (int step = 0; step < steps; step++)
            {
                currentPairsCount = new Dictionary<string, Int64>();
                foreach (var item in previousPairsCount)
                {
                    if (!currentPairsCount.TryAdd(string.Concat(item.Key[0], insertionPairs[item.Key]), item.Value))
                    {
                        currentPairsCount[string.Concat(item.Key[0], insertionPairs[item.Key])] += item.Value;
                    }
                    if (!currentPairsCount.TryAdd(string.Concat(insertionPairs[item.Key], item.Key[1]), item.Value))
                    {
                        currentPairsCount[string.Concat(insertionPairs[item.Key], item.Key[1])] += item.Value;
                    }
                }
                previousPairsCount = currentPairsCount;
            }
            return currentPairsCount;
        }

        private static Int64 CountDifferenceOccurences(Dictionary<string, Int64> polymer, char lastElementOfTemplate)
        {
            Dictionary<char, Int64> occurences = new Dictionary<char, Int64>();
            foreach (var item in polymer)
            {
                if (!occurences.TryAdd(item.Key[0], item.Value))
                {
                    occurences[item.Key[0]] += item.Value;
                }
            }
            // Only the last last element of the polymer is forgotten. But we can add it easily, it's the last element of the template.
            if (!occurences.TryAdd(lastElementOfTemplate, 1))
            {
                occurences[lastElementOfTemplate]++;
            }

            // Console.WriteLine("occurences0:" + occurences.First().Key + " " + occurences.First().ToString());
            // Console.WriteLine("occurences0:" + occurences.Last().Key + " " + occurences.Last().ToString());
            // Console.WriteLine(occurences.Max(x => x.Value).ToString());
            return occurences.Max(x => x.Value) - occurences.Min(x => x.Value);
        }
    }
}

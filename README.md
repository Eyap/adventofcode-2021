# AdventOfCode2021

My attempt at the [Advent of Code 2021](https://adventofcode.com/2021).
The puzzles are solved with C# and the help of the [AoCHelper](https://github.com/eduherminio/AoCHelper) template for ease of writting.


## Credits

Puzzles, Code, & Design of AdventOfCodes : [Eric Wastl](https://twitter.com/ericwastl)


## Documentation of AoCHelper

### Basic usage

- Create one class per advent day, following `DayXX` or `Day_XX` naming convention and implementing `AoCHelper.BaseDay`.
- Place input files under `Inputs/` dir, following `XX.txt` convention.
- Read the input content from `InputFilePath` and solve the puzzle by implementing `Solve_1()` and `Solve_2()`!

Invoking **different methods**:

- `Solver.SolveAll();` → solves all the days.

- `Solver.SolveLast();` → solves only the last day.

- `Solver.Solve<Day_XX>();` → solves only day `XX`.

- `Solver.Solve(new uint[] { XX, YY });` → solves only days `XX` and `YY`.

- `Solver.Solve(new [] { typeof(Day_XX), typeof(Day_YY) });` → same as above.

Providing a **custom `SolverConfiguration`** instance to any of those methods:

- `Solver.SolveLast(new SolverConfiguration() { ClearConsole = false } );` → solves only the last day providing a custom configuration.

- `Solver.SolveAll(new SolverConfiguration() { ElapsedTimeFormatSpecifier = "F3" } );` → solves all the days providing a custom configuration.

### Advanced usage

Check [AoCHelper README file](https://github.com/eduherminio/AoCHelper#advanced-usage) for detailed information about how to override the default file naming and location conventions of your problem classes and input files.
